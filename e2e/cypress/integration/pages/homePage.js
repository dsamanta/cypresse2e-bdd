/// <reference types="cypress" />



export default class homePage { 
    // Elements 
    buttonSearch(){
        return cy.get("button[type='submit']").eq(0);
    }

    inputBox(){
        return cy.get("input[name='search_query']");
    }

    successResult(){
        return cy.get("h1").find("span").eq(1);
    }
   
    
    //Action Methods : 
    validateTitle(title){
        cy.title(title);
    }

    clickSearchButton(){
        this.buttonSearch().click();
    }

    enterInput(inputString){
        this.inputBox().type(inputString);
    }

    validateSearchResult(){
        this.successResult().should('be.visible');
    }

    
    
}
