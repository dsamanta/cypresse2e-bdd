/// <reference types="cypress" />

import { Given,When,Then, And } from "cypress-cucumber-preprocessor/steps";
import homePage from '../pages/homePage';

const homepage = new homePage();




Given( 'Open the url', () => { 
    cy.visit( Cypress.env(Cypress.env().Env+'_url'));
    cy.wait(1000);
});

Then( 'Customer should land in Automation Practice page.Validate Title should be {string}', (title) => {    
    homepage.validateTitle(title);
} );

When( 'Enter Values to search {string}', (inputString) => {    
    homepage.enterInput(inputString)
} );

And('Click the Search button', () => {  
    homepage.clickSearchButton();
}); 

Then('Search Result should be displayed', () => {  
    homepage.validateSearchResult();
}); 
