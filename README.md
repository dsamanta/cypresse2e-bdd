<h2>Read Me !</h2>
<p>********************</p>
<p>This is a Cypress BDD POM design automation framework. Cypress is a front end testing tool to test modern web application. This framework uses BDD software engineering process to implement acceptance test driven developement. This automation framework also uses object repository design pattern to make to ensure no <span>code duplication and improves test maintenance.&nbsp;</span></p>
<p><strong>How to Run this Project in Cypress :</strong></p>
<ul>
<li>Open CLI or gitbash&nbsp;</li>
<li>Navigate to e2e folder&nbsp;</li>
<li>Run the command : <strong><span style="color: #2dc26b;"><em>npm run test&nbsp;</em></span></strong></li>
<li>This will open Cypress&nbsp;&nbsp;</li>
<li>Run the feature file&nbsp;</li>
<li>Once feature is completed , html reports [<span style="color: #e03e2d;"><em>cucumber-htmlreport.html</em></span>] generated saved in <span style="color: #e03e2d;"><em><strong>*.\cypresse2e-bdd\e2e\reports</strong></em></span></li>
</ul>
<p><span><strong>How to Run this Project in CLI:</strong></span></p>
<ul>
<li>Open CLI or gitbash&nbsp;</li>
<li>Navigate to e2e folder&nbsp;</li>
<li>Run the command : <strong><strong><span style="color: #2dc26b;"><em>npm run </em></span></strong><span style="color: #2dc26b;"><em>cy:feature</em></span></strong></li>
<li>This will run all the features designed under the folder : <span style="color: #2dc26b;"><strong>*.\cypress\integration</strong></span></li>
<li>Once feature is completed , html reports [<span style="color: #e03e2d;"><em>cucumber-htmlreport.html</em></span>] generated saved in <span style="color: #e03e2d;"><em><strong>*.\cypresse2e-bdd\e2e\reports</strong></em></span></li>
</ul>
<p></p>
<p></p>
<p><span style="text-decoration: underline;"><strong>How to setup new Cypress-cucumber Project :&nbsp;</strong></span></p>
<p><strong>Steps :&nbsp;</strong></p>
<p><strong>Install Cypress :&nbsp;</strong></p>
<ul>
<li>Create a new folder</li>
<li>Open CLI or gitbash&nbsp;</li>
<li>Navigate to the newly created folder in cli&nbsp;</li>
<li>Create a Package.JSON file. Run the below mentioned command</li>
<li><span style="color: #2dc26b;"><strong>npm init -y</strong></span> {<em><strong>npm</strong>&nbsp;<strong>init</strong>&nbsp;-<strong>y</strong></em><span><em>&nbsp;will simply generate an empty npm project without going through an interactive process.</em>}</span></li>
<li>
<pre class="language-javascript"><code>npm init -y</code></pre>
</li>
<li><span>Install cypress locally or globally&nbsp;</span>
<ul>
<li><span>Locally : <strong>npm install cypress --save-dev</strong></span></li>
<li><span><strong>or</strong></span></li>
<li>Globally : <span><strong>npm install cypress</strong></span>&nbsp;</li>
<li>
<pre class="language-javascript"><code>npm install cypress --save-dev
or 
npm install cypress </code></pre>
</li>
</ul>
</li>
<li>Update package.JSON script - <em style="font-size: 1rem;"><em style="font-size: 1rem;">"scripts": {"test" :"cypress open"}</em></em></li>
</ul>
<div>
<ul>
<li>Open CLI or gitbash - <em>Run npm run test</em></li>
<li>This will open Cypress and also create the cypress folder</li>
</ul>
<p><strong>Install Cypress-cucumber :&nbsp;</strong></p>
<ul>
<li><strong>Install cypress cucumber locally or globally&nbsp;</strong></li>
<li>
<pre class="language-javascript"><code>npm install --save-dev cypress-cucumber-preprocessor
or
npm i cypress-cucumber-preprocessor</code></pre>
</li>
<li>Update <strong>package.JSON&nbsp;</strong>
<ul>
<li>
<div><span>Add the below setion in package "cypress-cucumber-preprocessor"</span><span>: </span><span style="color: #2dc26b;"><em></em></span></div>
<div>
<pre class="language-javascript"><code>{ "nonGlobalStepDefinitions": true } </code></pre>
</div>
</li>
</ul>
</li>
<li>Update <strong>index.js</strong>
<ul>
<li>Navigate to :&nbsp; <strong>.*\cypress\plugins\index.js</strong></li>
<li>Add the below mentioned section to the file&nbsp;</li>
<li>
<pre class="language-javascript"><code>}
const cucumber = require('cypress-cucumber-preprocessor').default

module.exports = (on, config) =&gt; {
  on('file:preprocessor', cucumber())
}</code></pre>
</li>
</ul>
</li>
</ul>
</div>
<ul>
<li>Update <strong>cypress.json</strong>
<ul>
<li>
<pre class="language-javascript"><code>{
  "testFiles": "**/*.feature"
}</code></pre>
</li>
</ul>
</li>
</ul>
<p></p>
<p><em>Note # Refer to my repo for creating , running feature file and stepDefinitions.&nbsp;</em></p>